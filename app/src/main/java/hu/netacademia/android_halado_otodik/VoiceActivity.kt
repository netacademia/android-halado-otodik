package hu.netacademia.android_halado_otodik

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.speech.tts.TextToSpeech
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.SparseArray
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.text.TextBlock
import com.google.android.gms.vision.text.TextRecognizer
import java.io.IOException
import java.util.*

class VoiceActivity : AppCompatActivity() {

    private lateinit var textToSpeech: TextToSpeech

    private var cameraSource: CameraSource? = null

    private var cameraView: SurfaceView? = null
    private var recognizedText: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voice)

        initViews()
        initOcrEngine()
    }

    private fun initViews() {
        cameraView = findViewById(R.id.voice_camera_view)
        recognizedText = findViewById(R.id.voice_recognized_text)
    }

    private fun initOcrEngine() {
        val textRecognizer = TextRecognizer.Builder(this).build()

        if (textRecognizer.isOperational) {
            initTtsEngine()
            initCamera(textRecognizer)
            initTextProcessor(textRecognizer)
        } else {
            Toast.makeText(this, getString(R.string.ocr_engine_init_error), Toast.LENGTH_LONG).show()
        }
    }

    private fun initTtsEngine() {
        textToSpeech = TextToSpeech(this, TextToSpeech.OnInitListener { status ->
            if (status != TextToSpeech.ERROR) {
                textToSpeech.language = Locale.US
            }
        })
    }

    private fun initCamera(textRecognizer: TextRecognizer) {
        requestCameraPermissions()
        startCameraSource(textRecognizer)
        startCameraHolder()
    }

    private fun requestCameraPermissions() {
        if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@VoiceActivity, arrayOf(Manifest.permission.CAMERA), 0)
        }
    }

    private fun startCameraSource(textRecognizer: TextRecognizer) {
        cameraSource = CameraSource.Builder(this, textRecognizer)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1280, 1024)
                .setRequestedFps(2.0f)
                .setAutoFocusEnabled(true)
                .build()
    }

    private fun startCameraHolder() {
        cameraView?.holder?.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceChanged(p0: SurfaceHolder?, p1: Int, p2: Int, p3: Int) {
            }

            override fun surfaceDestroyed(holder: SurfaceHolder?) {
                cameraSource?.stop()
            }

            @SuppressLint("MissingPermission")
            override fun surfaceCreated(holder: SurfaceHolder?) {
                try {
                    cameraSource?.start(cameraView?.holder)
                } catch (ex: IOException) {
                    ex.printStackTrace()
                }
            }

        })
    }

    private fun initTextProcessor(textRecognizer: TextRecognizer?) {
        textRecognizer?.setProcessor(object : Detector.Processor<TextBlock> {
            override fun release() {
            }

            override fun receiveDetections(detections: Detector.Detections<TextBlock>?) {
                val items = detections?.detectedItems
                items?.let { recognizedItems ->
                    if (recognizedItems.size() > 0) {
                        parseItems(recognizedItems)
                    }
                }
            }
        })
    }

    private fun parseItems(recognizedItems: SparseArray<TextBlock>) {
        recognizedText?.post {

            val stringBuilder = StringBuilder()
            for (i in 0 until recognizedItems.size()) {
                val recognizedItem = recognizedItems.valueAt(i)
                stringBuilder.append(recognizedItem.value)
            }
            val textToShow = stringBuilder.toString()

            recognizedText?.text = textToShow
            textToSpeech.speak(textToShow, TextToSpeech.QUEUE_FLUSH, null, UUID.randomUUID().toString())
        }
    }

    override fun onPause() {
        textToSpeech.stop()
        textToSpeech.shutdown()
        super.onPause()
    }
}