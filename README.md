# Android-Halado-Otodik

A videó során meglátogatott oldalak

## Global
* Android dashboard https://developer.android.com/about/dashboards/

## Libek, komponensek
* TTS engine - https://developer.android.com/reference/android/speech/tts/TextToSpeech
* Firebase ML kit - https://firebase.google.com/docs/ml-kit/android/recognize-text
* Google Mobile Vision - https://developers.google.com/vision/

## Guide oldalak

* OCR reader sample app - https://github.com/googlesamples/android-vision/tree/master/visionSamples/ocr-reader
* TTS használata - https://www.tutorialspoint.com/android/android_text_to_speech.htm 
* Kamera intent https://developer.android.com/training/camera/photobasics#TaskCaptureIntent


## Egyéb

* InitListener TTS engine https://developer.android.com/reference/android/speech/tts/TextToSpeech.OnInitListener
* Említett kamera lib - Fottoaparat https://github.com/RedApparat/Fotoapparat